//
//  TradeMeTest1Tests.swift
//  TradeMeTest1Tests
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import XCTest
@testable import TradeMeTest1


class TradeMeTest1Tests: XCTestCase {
    
    var sut : TradeMeTest1RestAPI!
    
    override func setUp() {
        sut = TradeMeTest1RestAPI.sharedInstance
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testRefreshToken() {
        
        let expectation = self.expectation(description: "User")
        var userFound : User? = nil
        
        sut.requestToken(consumerKey: "A1AC63F0332A131A78FAC304D007E7D1",
                         signature: "EC7F18B17A062962C6930A8AE88B16C7",
                         onResponse: { (user) in
                            
                            userFound = user
                            expectation.fulfill()
                            
        }) { (error) in
            
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotNil(userFound, "requestToken ")
    }
    
    func testGetCategories() {
        
        let expectation = self.expectation(description: "Categories")
        var count = 0
        
        sut.getCategories(rootCategory: 0,
                          depth: 1,
                          onResponse:
            { (categories) in
                
                count = categories.count
                expectation.fulfill()
                
        }) { (error) in
            
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(count>0, "getCategories rootCategory:0")
    }
    
    func testGetCategoryDetail() {
        
        let expectation = self.expectation(description: "CategoriesSearch")
        var count = 0
        
        sut.getDetailForCategories(categories: ["0001-","0187-","0193-"],
                                   rows: 3,
                                   onResponse:
            { (items) in
                
                count = items.count
                expectation.fulfill()
                
        }) { (error) in
            
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(count>0, "getDetailForCategories categories:0001-0187-0193-")
    }
    
    func testGetListing() {
        
        let expectation = self.expectation(description: "CategoriesSearch")
        var id = 0
        
        sut.getListing(listingId: 6904378,
                       onResponse:
            { (listDetail) in
                id = listDetail.listingId
                expectation.fulfill()
        }) { (error) in
            
        }
        
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssert(id>0, "getListing listingId: 6904378")
    }
    
    /*
     func testPerformanceExample() {
     // This is an example of a performance test case.
     self.measure {
     // Put the code you want to measure the time of here.
     }
     }
     */
    
}
