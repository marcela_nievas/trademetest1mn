//
//  TradeMeTest1UITests.swift
//  TradeMeTest1UITests
//
//  Created by Marcela Nievas on 09/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import XCTest

class TradeMeTest1UITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testiPhoneNavigation() {
        
        // Categories
        XCUIDevice.shared.orientation = .faceUp
        let categoryTable = XCUIApplication().tables
        XCTAssert(categoryTable.count > 0)
        
        categoryTable.staticTexts["Building & renovation"].swipeUp()
        categoryTable.staticTexts["Electronics & photography"].swipeUp()
        categoryTable.staticTexts["Toys & models"].tap()
        
        // Listing Item
        
        let collectionViewsQuery = app.collectionViews
        XCTAssert(collectionViewsQuery.count > 0)
        
        let cell7 = collectionViewsQuery.children(matching: .cell).element(boundBy: 7)
        expectation(for: NSPredicate(format: "exists == 1"),
                    evaluatedWith: cell7,
                    handler:nil)
        waitForExpectations(timeout: 5, handler: nil)
        cell7.tap()
        
        // go back

        app.navigationBars["Categories"].buttons["Toys & models"].tap()
        
        let collectionViewsQuery2 = app.collectionViews
        collectionViewsQuery2.children(matching: .cell).firstMatch.swipeUp()
        
        app.navigationBars["Toys & models"].buttons["Categories"].tap()

    }
    
}
