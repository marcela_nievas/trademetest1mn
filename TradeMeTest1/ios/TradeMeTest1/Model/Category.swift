//
//  Category.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

struct RootCategory : Decodable {
    
    let categories : [Category]
    
    init(categories: [Category]) {
        self.categories = categories
    }
    
    private enum RootCategoryKey: String, CodingKey {
        case subcategories = "Subcategories"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootCategoryKey.self)
        let categories: [Category] = try container.decode([Category].self, forKey: .subcategories)
        self.init(categories:categories)
    }
}

struct Category: Decodable  {

    let name: String
    let number: String
    let path: String
    let subcategories : [Subcategory]
    
    enum CategoryKeys: String, CodingKey {
        case name = "Name"
        case number = "Number"
        case path = "Path"
        case subcategories = "Subcategories"
    }
    
    init(name: String, number: String, path: String, subcategories : [Subcategory]) {
        self.name = name
        self.number = number
        self.path = path
        self.subcategories = subcategories
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CategoryKeys.self)
        
        let name: String = try container.decode(String.self, forKey: .name)
        let number: String = try container.decode(String.self, forKey: .number)
        let path: String = try container.decode(String.self, forKey: .path)
        var subcategories : [Subcategory]? = try? container.decode([Subcategory].self, forKey: .subcategories)
        
        if subcategories == nil {
            subcategories = [Subcategory]()
        }
        self.init(name:name, number:number, path:path, subcategories:subcategories!)
    }

    /*
     {
     "Name": "Root",
     "Number": "",
     "Path": "",
     "Subcategories": [
     {
         "Name": "Trade Me Motors",
         "Number": "0001-",
         "Path": "/Trade-Me-Motors",
         "HasClassifieds": true,
         "CanHaveSecondCategory": true,
         "CanBeSecondCategory": true,
         "IsLeaf": false
        },
     {
         "Name": "Trade Me Property",
         "Number": "0350-",
         "Path": "/Trade-Me-Property",
         "HasClassifieds": true,
         "IsLeaf": false
     },
     */
    
}

struct Subcategory: Decodable  {
    
    let name: String
    let number: String
    let path: String
    let isLeaf : Bool
    
    enum CategoryKeys: String, CodingKey {
        case name = "Name"
        case number = "Number"
        case path = "Path"
        case isLeaf = "IsLeaf"
    }
    /*
     "Subcategories": [
     {
     "Name": "Glass & crystal",
     "Number": "0340-2650-",
     "Path": "/Pottery-glass/Glass-crystal",
     "CanHaveSecondCategory": true,
     "CanBeSecondCategory": true,
     "IsLeaf": false
     },
     
     */
    
    init(name: String, number: String, path: String, isLeaf : Bool) {
        self.name = name
        self.number = number
        self.path = path
        self.isLeaf = isLeaf
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CategoryKeys.self)
        
        let name: String = try container.decode(String.self, forKey: .name)
        let number: String = try container.decode(String.self, forKey: .number)
        let path: String = try container.decode(String.self, forKey: .path)
        let isLeaf : Bool = try container.decode(Bool.self, forKey: .isLeaf)
        
        self.init(name:name, number:number, path:path, isLeaf:isLeaf)
    }
    
    /*
     {
     "Name": "Root",
     "Number": "",
     "Path": "",
     "Subcategories": [
     {
     "Name": "Trade Me Motors",
     "Number": "0001-",
     "Path": "/Trade-Me-Motors",
     "HasClassifieds": true,
     "CanHaveSecondCategory": true,
     "CanBeSecondCategory": true,
     "IsLeaf": false
     },
     {
     "Name": "Trade Me Property",
     "Number": "0350-",
     "Path": "/Trade-Me-Property",
     "HasClassifieds": true,
     "IsLeaf": false
     },
     */
    
    
}

