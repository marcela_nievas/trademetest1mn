//
//  User.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var token : String?
    var secret : String?
    
    init(token : String, secret : String) {
        self.token = token
        self.secret = secret
    }
    
}
