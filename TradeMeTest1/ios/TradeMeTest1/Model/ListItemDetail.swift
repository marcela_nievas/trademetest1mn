//
//  ListItemDetail.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 08/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//


import UIKit

// TODO research about how to use DECODABLE with this particular json format


class ListItemDetail  {
    
    var listingId: Int = 0
    var title: String = ""
    var subtitle: String = ""
    var body: String = ""
    var suburb: String = ""
    var allowsPickups: String = ""
    var photoId: Int = 0
    var photos = [ListItemPhoto]()
    
    var priceDisplay: String? = nil
    var isNew = false
    
    init?(json : JSONDictionary) {
        
        /*
         "AllowsPickups": 2,
         "PriceDisplay": $1.00,
         "CategoryName": Couplings & hitches,
         "PaymentOptions": Cash,
         "Subtitle": SKYLARC MASSIVE LIGHTING LIQUIDATION,
         "Suburb": Christchurch City,
         "Body": "SKYLARC ASSET REALISATION\r\nSave us....
         Photos": [
         { ...}
         ],
         
         */
        
        for (key,value) in json {
//            TRACE("(\(key),\(value))")
            
            switch (key) {
            case "Title":           if let temp = value as? String     { self.title = temp }
            case "Body":            if let temp = value as? String     { self.body = temp }
            case "ListingId":       if let temp = value as? Int        { self.listingId = temp }
            case "PriceDisplay":    if let temp = value as? String     { self.priceDisplay = temp }
            case "Suburb":          if let temp = value as? String     { self.suburb = temp }
            case "AllowsPickups":   if let temp = value as? String     { self.allowsPickups = temp }
            case "PhotoId":         if let temp = value as? Int        { self.photoId = temp }
            case "IsNew":           if let temp = value as? Bool       { self.isNew = temp }
            
            case "Photos":
                if let photos = value as? [JSONDictionary] {
                    for jsonPhoto in photos {
                        let p = ListItemPhoto.init(json:jsonPhoto)
                        self.photos.append(p)
                    }
                }
                
            default:
                break
            }
        }
        
        if self.listingId == 0 {
            return nil
        }
    }
}

struct ListItemPhoto {
    
    var photoId: Int = 0
    var valuePhoto: PhotoValues? = nil
    
    init(json : JSONDictionary) {
        
        /*
         "Key": 1074908,
         "Value": {
            ...
         }
         */
        
        for (key,value) in json {
//            TRACE("(\(key),\(value))")
            
            switch (key) {
            case "Key":           if let temp = value as? Int     { self.photoId = temp }

            case "Value":
                if let values = value as? JSONDictionary {
                    let v = PhotoValues.init(json: values)
                    self.valuePhoto = v
                }
                
            default:
                break
            }
        }
    }

}

struct PhotoValues  {
    
    var thumbnail: String? = nil
    var medium: String? = nil
    var large: String? = nil
    var fullSize: String? = nil
    
    /*
     "Value": {
     "Thumbnail": "https://images.tmsandbox.co.nz/photoserver/thumb/1074908.jpg",
     "List": "https://images.tmsandbox.co.nz/photoserver/lv2/1074908.jpg",
     "Medium": "https://images.tmsandbox.co.nz/photoserver/med/1074908.jpg",
     "Gallery": "https://images.tmsandbox.co.nz/photoserver/gv/1074908.jpg",
     "Large": "https://images.tmsandbox.co.nz/photoserver/tq/1074908.jpg",
     "FullSize": "https://images.tmsandbox.co.nz/photoserver/full/1074908.jpg",
     "PhotoId": 1074908,
     "OriginalWidth": 427,
     "OriginalHeight": 650
     }
     */
    
    init(json : JSONDictionary) {
        
        for (key,value) in json {
//            TRACE("(\(key),\(value))")
            
            switch (key) {
            case "Thumbnail":        if let temp = value as? String     { self.thumbnail = temp }
            case "Medium":           if let temp = value as? String     { self.medium = temp }
            case "Large":            if let temp = value as? String     { self.large = temp }
            case "FullSize":         if let temp = value as? String     { self.fullSize = temp }
                
            default:
                break
            }
        }
    }
    
}
