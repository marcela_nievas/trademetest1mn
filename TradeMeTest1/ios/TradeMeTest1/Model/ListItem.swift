//
//  ListItem.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 07/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

struct RootListItem : Decodable {
    
    let list : [ListItem]
    
    init(items: [ListItem]) {
        self.list = items
    }
    
    private enum RootListItemKey: String, CodingKey {
        case list = "List"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootListItemKey.self)
        let items: [ListItem] = try container.decode([ListItem].self, forKey: .list)
        self.init(items:items)
    }
}

struct ListItem: Decodable {

    let listId: Int
    let title: String
    let thumbnail: String?

//    "ListingId": 6904378,
//    "Title": "Abstract 03",
//    "PictureHref": "https://images.tmsandbox.co.nz/photoserver/lv2/1074908.jpg",
    
        enum ListItemKeys: String, CodingKey {
            case listId = "ListingId"
            case title = "Title"
            case picture = "PictureHref"
        }
        
        init(listId: Int, title: String, thumbnail: String?=nil) {
            self.listId = listId
            self.title = title
            self.thumbnail = thumbnail
        }
        
        init(from decoder: Decoder) throws {
            
            let container = try decoder.container(keyedBy: ListItemKeys.self)
            
            let listId: Int = try container.decode(Int.self, forKey: .listId)
            let title: String = try container.decode(String.self, forKey: .title)
            let thumbnail: String? = try? container.decode(String.self, forKey: .picture)
            
            self.init(listId:listId, title:title, thumbnail:thumbnail)
        }
}

    /*
     {
     "TotalCount": 152,
     "Page": 1,
     "PageSize": 3,
     "List": [
     {
         "ListingId": 6904378,
         "Title": "Abstract 03",
         "Category": "0339-0066-1732-7346-",
         "StartPrice": 154.5,
         "BuyNowPrice": 303.85,
         "StartDate": "/Date(1541103149797)/",
         "EndDate": "/Date(1541707860000)/",
         "ListingLength": null,
         "IsFeatured": true,
         "HasGallery": true,
         "IsBold": true,
         "AsAt": "/Date(1541629615123)/",
         "CategoryPath": "/Art/Paintings/Other/Acrylic",
         "PictureHref": "https://images.tmsandbox.co.nz/photoserver/lv2/1074908.jpg",
         "IsNew": true,
         "Region": "Wairarapa",
         "Suburb": "Martinborough",
         "HasReserve": true,
         "HasBuyNow": true,
         "NoteDate": "/Date(0)/",
         "ReserveState": 2,
         "PriceDisplay": "$154.50",
         "PromotionId": 4,
         "AdditionalData": {
         "BulletPoints": [],
         "Tags": []
         },
         "MemberId": 4000129
     },
     {
         "ListingId": 6904470,
         "Title": "Abstract 09 a1",
         "Category": "0339-1733-3013-",
         "StartPrice": 163.5,
         "BuyNowPrice": 260,
         "StartDate": "/Date(1541115616860)/",
         "EndDate": "/Date(1541720340000)/",
         "ListingLength": null,
         "IsFeatured": true,
         "HasGallery": true,
         "IsBold": true,
         "MaxBidAmount": 170.5,
         "AsAt": "/Date(1541629615123)/",
         "CategoryPath": "/Art/Prints/Abstract",
         "PictureHref": "https://images.tmsandbox.co.nz/photoserver/lv2/1105898.jpg",
         "IsNew": true,
         "Region": "Wairarapa",
         "Suburb": "Martinborough",
         "BidCount": 8,
         "HasReserve": true,
         "HasBuyNow": true,
         "NoteDate": "/Date(0)/",
         "ReserveState": 2,
         "Subtitle": "abstract",
         "PriceDisplay": "$170.50",
         "PromotionId": 4,
         "AdditionalData": {
         "BulletPoints": [],
         "Tags": []
         },
         "MemberId": 4000129
         },
     */

