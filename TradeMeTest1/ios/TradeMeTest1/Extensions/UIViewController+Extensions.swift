//
//  UIViewController+Extensions.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 07/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

extension UIViewController {
    
    class func displaySpinner(onView : UIView, alpha: CGFloat = 0.5) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red:0, green: 0, blue: 0, alpha: alpha)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    func presentOkAlert(title:String = "", message:String) {
        
        let ac = UIAlertController(title: title,
                                   message: message,
                                   preferredStyle: .alert)
        
        ac.addAction(UIAlertAction(title: kOk, style: .default))
        self.present(ac, animated: true)
    }
    
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    func presentMessageError(error : TMTest1Error) {
        
        var msg = kDefaultErrorMsg
        switch (error) {
        case .networkIsNotReachable:
            msg = kNoConnectivityDefaultError
        default:
            break
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController?.presentOkAlert(message: msg)
    }
}
