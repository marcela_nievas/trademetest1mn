//
//  String+Extensions.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 07/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import Foundation

extension String {

    static func randomAlphaNumericString(length: Int) -> String {
        let charactersString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let charactersArray : [Character] = Array(charactersString)
        
        var string = ""
        for _ in 0..<length {
            string.append(charactersArray[Int(arc4random()) % charactersArray.count])
        }
        
        return string
    }
}

