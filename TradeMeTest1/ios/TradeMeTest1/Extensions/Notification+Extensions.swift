//
//  Notification+Extensions.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 09/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let networkReachabilityIsReachable = Notification.Name("network.is.reachable")
    static let networkReachabilityIsNotReachable = Notification.Name("network.is.not.reachable")
}

