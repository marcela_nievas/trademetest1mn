//
//  KeysManager.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class KeysManager: NSObject {

    static let sharedInstance = KeysManager()
    
    enum kKeysManager : String {
        case oauthToken = "oauth_token"
        case oauthTokenSecret = "oauth_token_secret"
    }
    
    var usersToken : String {
        get {
            guard let token = KeychainWrapper.standard.string(forKey: kKeysManager.oauthToken.rawValue) else {
                return ""
            }
            return token
        }
        
        set (value) {
            let saveSuccessful : Bool = KeychainWrapper.standard.set(value, forKey: kKeysManager.oauthToken.rawValue)
            assert(saveSuccessful)
        }
    }
    
    var usersTokenSecret : String {
        get {
            guard let token = KeychainWrapper.standard.string(forKey: kKeysManager.oauthTokenSecret.rawValue) else {
                return ""
            }
            return token
        }
        
        set (value) {
            let saveSuccessful : Bool = KeychainWrapper.standard.set(value, forKey: kKeysManager.oauthTokenSecret.rawValue)
            assert(saveSuccessful)
        }
    }

}
