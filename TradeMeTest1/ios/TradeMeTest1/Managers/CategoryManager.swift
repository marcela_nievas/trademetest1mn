//
//  CategoryManager.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class CategoryManager: NSObject {

    var categories = [Category]()
    
    static let sharedInstance = CategoryManager()
    
    
    func refreshCategories(onFinish:@escaping ([Category]?) -> ()) {
        
        TradeMeTest1RestAPI.sharedInstance.getCategories(rootCategory:String(0),
                                                         depth:1,
                                                         onResponse:
            { (categories) in
            
                self.categories = categories
                onFinish(self.categories)
                
        }) { (error) in
            self.categories.removeAll()
            onFinish(self.categories)
        }
        
    }
    
}
