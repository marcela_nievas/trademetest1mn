//
//  SessionManager.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class SessionManager: NSObject {
    
    let fistInstallToken = "FirstInstall"
    static let sharedInstance = SessionManager()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override init() {
        super.init()
        let nc = NotificationCenter.default
        
        nc.addObserver(self,
                       selector: #selector(networkIsReachableNow),
                       name: .networkReachabilityIsReachable,
                       object: nil)
        
        nc.addObserver(self,
                       selector: #selector(networkIsNotReachableNow),
                       name: .networkReachabilityIsNotReachable,
                       object: nil)
    }
    
    @objc func networkIsReachableNow(){
        if !self.isLogged {
            resfreshSession()
        }
    }
    
    @objc func networkIsNotReachableNow(){
        
    }
    
    public var isLogged : Bool {
        get {
            TRACE("token:\(KeysManager.sharedInstance.usersToken) secret:\(KeysManager.sharedInstance.usersTokenSecret)")
            return KeysManager.sharedInstance.usersToken.count > 0 &&
                KeysManager.sharedInstance.usersTokenSecret.count > 0
        }
    }
    
    func resfreshSession() {
        
        // TODO: complete the full login process
        // https://developer.trademe.co.nz/api-overview/registering-an-application/
        
        KeysManager.sharedInstance.usersToken = ""
        KeysManager.sharedInstance.usersTokenSecret = ""
        
        self.loginWithDefaultCredentialsOnFinished(onSuccess: { (user) in
            TRACE("the token has been refreshed")
            
        }) { (error) in
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController?.presentMessageError(error: error)
        }
    }
    
    func loginWithDefaultCredentialsOnFinished(onSuccess : @escaping (User) -> (),
                                               onFailure : @escaping (TMTest1Error) -> ()) {
        
        TradeMeTest1RestAPI.sharedInstance.requestToken(consumerKey: kDefaultConsumerKey,
                                                        signature: kDefaultSignature,
                                                        onResponse:
            { user in
                
                if let token = user.token, let secret = user.secret {
                    KeysManager.sharedInstance.usersToken = token
                    KeysManager.sharedInstance.usersTokenSecret = secret
                }
                
                onSuccess(user)
        })
        { error in
            onFailure(error as! TMTest1Error)
        }
    }
    
    
}
