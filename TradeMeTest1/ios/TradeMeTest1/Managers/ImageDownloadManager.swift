//
//  ImageDownloadManager.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 07/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

// base on source
// http://www.tekramer.com/downloading-caching-and-decoding-images-asynchronously-with-alamofire-part-1-swift-4

extension UInt64 {
    func megabytes() -> UInt64 {
        return self * 1024 * 1024
    }
}

class ImageDownloadManager: NSObject {

    static let sharedInstance = ImageDownloadManager()
    
    let imageCache = AutoPurgingImageCache(
        memoryCapacity: UInt64(50).megabytes(),
        preferredMemoryUsageAfterPurge: UInt64(20).megabytes()
    )
    
    func retrieveImage(for url: String,
                       onCompletion: @escaping (UIImage) -> Void,
                       onFailure: @escaping () -> Void
                       ) -> Request {
        return Alamofire.request(url, method: .get).responseImage { response in
            guard let image = response.result.value else {
                onFailure()
                return
            }
            onCompletion(image)
            self.cache(image, for: url)
        }
    }
    
    //MARK: - Image Caching
    
    private func cache(_ image: Image, for url: String) {
        imageCache.add(image, withIdentifier: url)
    }
    
    func cachedImage(for url: String) -> Image? {
        return imageCache.image(withIdentifier: url)
    }
    
}
