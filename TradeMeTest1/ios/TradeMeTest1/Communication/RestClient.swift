//
//  RestClient.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit
import Alamofire

// base on https://medium.com/@kamilm/rest-api-design-with-alamofire-promisekit-swift-f953e5bf1def

public enum ServerURL: String {
    case oauth = "secure.tmsandbox.co.nz/Oauth"
    case base = "api.tmsandbox.co.nz/v1"
}

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

public enum Resource {
    case requestToken
    case getCategories
    case searchInCategory
    case getListingItemDetail
    
    public var resource: (method: HTTPMethod, route: String) {
        switch self {
        case .requestToken:         return (.post, "/RequestToken")
        case .getCategories:        return (.get, "/Categories")
        case .searchInCategory:     return (.get, "/Search/General")
        case .getListingItemDetail: return (.get, "/Listings")
            
            // getCategories
            // https://api.tmsandbox.co.nz/v1/Categories/0.json?depth=1
            
            // searchInCategory
            // https://api.tmsandbox.co.nz/v1/Search/General.json?buy=All&category=0339-&clearance=All&condition=All&expired=false&listed_as=All&page=1&pay=All&photo_size=List&return_collections=false&return_did_you_mean=false&return_metadata=false&rows=20&seller_status=All&shipping_method=All&sort_order=Default
            
            // getListingItemDetail
            // GET https://api.tmsandbox.co.nz/v1/Listings/6903735.json?increment_view_count=false&question_limit=20&return_member_profile=false
            
        }
    }
}

public class RestClient {
    var baseURL: String
    
    static let defaultHeader: HTTPHeaders = [
        "Content-Type": "application/json"
    ]
    
    public init(baseURL: ServerURL = .base) {
        self.baseURL = baseURL.rawValue
    }
    
    static let oauth1DefaultHeader: HTTPHeaders = [
        "oauth_signature_method": "PLAINTEXT",
        "oauth_version": "1.0",
        ]
    
    /*
     Authorization: OAuth
     oauth_callback=https://www.website-tm-access.co.nz/trademe-callback,
     oauth_consumer_key=4E0D082355116884742E5F33B8A199F411,
     oauth_version=1.0,
     oauth_timestamp=1285532322,
     oauth_nonce=7O3kEe,
     oauth_signature_method=PLAINTEXT,
     oauth_signature=160FCF77971DC92A38596288DB071A8CA5%26
     */
    static func requestTokenHeader(consumerKey:String, signature:String) -> HTTPHeaders {
        
        var tempHeader = RestClient.oauth1DefaultHeader
        
        //tempHeader["oauth_callback"] = "tmt1://trademe-callback"
        tempHeader["oauth_consumer_key"] = consumerKey
        tempHeader["oauth_signature"] = signature + "%26"
        tempHeader["oauth_timestamp"] = String(Int(NSDate().timeIntervalSince1970))
        tempHeader["oauth_nonce"] = String.randomAlphaNumericString(length: 6)
        
        let authorizationHeader = tempHeader.reduce("OAuth") { (previous, value) -> String in
            return previous + " \(value.key)=\"\(value.value)\", "
        }
        var header = RestClient.defaultHeader
        
        header["Authorization"] = authorizationHeader
        header["Content-Type"] = "application/x-www-form-urlencoded"
        
        TRACE("\(header)")
        return header
    }
    
    
    
    static func authorizedHeader(consumerKey:String,
                                 signature:String,
                                 usersToken : String? = nil,
                                 usersTokenSecret: String? = nil) -> HTTPHeaders {
        
        var tempHeader = RestClient.oauth1DefaultHeader
        tempHeader["oauth_consumer_key"] = consumerKey
        tempHeader["oauth_signature"] = signature + "%26"
        tempHeader["oauth_timestamp"] = String(Int(NSDate().timeIntervalSince1970))
        tempHeader["oauth_nonce"] = String.randomAlphaNumericString(length: 6)
        
        // TODO finish full login process
        
//        tempHeader["oauth_signature"] = signature + "%26"
//        tempHeader["oauth_signature"] = signature + "%26"
        
        let authorizationHeader = tempHeader.reduce("OAuth") { (previous, value) -> String in
            return previous + " \(value.key)=\"\(value.value)\", "
        }
        var header = RestClient.defaultHeader
        
        header["Authorization"] = authorizationHeader
        header["Content-Type"] = "application/x-www-form-urlencoded"
        
        return header
    }
    
}

