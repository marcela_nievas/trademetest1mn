//
//  RestAPIManager.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import Foundation
import Alamofire

typealias JSONDictionary = [String: Any]

public enum TMTest1Error: Error {
//    case invalidCredentials
    case networkIsNotReachable
    case dataFileNil
    case networkError(error:Error)
}

class RestAPIManager {
    
    static let sharedInstance = RestAPIManager()
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "google.com")
    
    var isReachable : Bool? {
        get {
            return reachabilityManager?.isReachable
        }
    }
    
    //MARK: - Life Cycle
    
    init() {
        startNetworkReachabilityObserver()
    }

    private func startNetworkReachabilityObserver() {
        
        reachabilityManager?.listener = { status in
            switch status {
            case .unknown :
                TRACE("It is unknown whether the network is reachable")

            case .notReachable:
                TRACE("The network is not reachable")
                NotificationCenter.default.post(name:.networkReachabilityIsNotReachable,
                                                object: nil, userInfo: nil)
                
            case .reachable(.ethernetOrWiFi):
                fallthrough
            case .reachable(.wwan):
                TRACE("The network is reachable over the WiFi or WWAN connection")
                NotificationCenter.default.post(name:.networkReachabilityIsReachable,
                                                object: nil, userInfo: nil)
                
            }
        }
        
        // start listening
        reachabilityManager?.startListening()
    }
    
    //MARK: - Public
    
    public func requestData(resource: Resource,
                            scheme: String = "https",
                            baseUrl: String,
                            extraUrl: String = "",
                            headerParameters: [String: String] = [:],
                            bodyParameters: [String: Any] = [:],
                            headers: HTTPHeaders = [:],
                            onResponse: @escaping (Data) -> (),
                            onFailure: @escaping (TMTest1Error) -> ()) {
        
        let method = resource.resource.method
        let url = buildURL(resource: resource, scheme: scheme, baseUrl: baseUrl, extraUrl: extraUrl, headerParameters: headerParameters)
        
        Alamofire.request(url,
                          method: method,
                          parameters: method == .get ? nil : bodyParameters,
                          encoding: URLEncoding.default,
                          headers: headers)
            
            .response { (response) in
                
                TRACE("--- REST API ---")
                TRACE("\(method.rawValue) \(response.request!)")
                TRACE("bodyParameters:\(bodyParameters)")
                
                if let data = response.data  {
                    onResponse(data)
                } else {
                    onFailure(TMTest1Error.dataFileNil)
                }
                
        }
    }
    
    public func requestJson(resource: Resource,
                            scheme: String = "https",
                            baseUrl: String,
                            extraUrl: String = "",
                            headerParameters: [String: String] = [:],
                            bodyParameters: [String: Any] = [:],
                            headers: HTTPHeaders = [:],
                            onResponse: @escaping (JSONDictionary) -> (),
                            onFailure: @escaping (TMTest1Error) -> ()) {
        
        let method = resource.resource.method
        let url = buildURL(resource: resource, scheme: scheme, baseUrl: baseUrl, extraUrl: extraUrl, headerParameters: headerParameters)
        
        Alamofire.request(url,
                          method: method,
                          parameters: method == .get ? nil : bodyParameters,
                          encoding: URLEncoding.default,
                          headers: headers)
            
            .responseJSON { (response) in
                
                TRACE("--- REST API ---")
                TRACE("\(method.rawValue) \(response.request!)")
                TRACE("bodyParameters:\(bodyParameters)")
                
                switch response.result {
                    
                case .success(let json):
                    guard let json = json as? JSONDictionary else {
                        onFailure(TMTest1Error.dataFileNil)
                        return
                    }
                    //TRACE("\(json)")
                    onResponse(json)
                    
                case .failure(let error):
                    TRACE("\(error)")
                    onFailure(TMTest1Error.networkError(error: error))
                }
                
        }
    }
    
    //MARK: - Private
    
    private class func extractTokenForHeader(header:String, str:String) -> String? {
        
        let range = NSRange(str.startIndex..., in: str)
        var token: String? = nil
        let headerKey = "\(header)="
        if let regex = try? NSRegularExpression(pattern: "\(headerKey)([a-zA-Z0-9])*", options: .caseInsensitive),
            let match = regex.firstMatch(in: str, range: range) {
            
            token = String(str[Range(match.range, in: str)!]).replacingOccurrences(of: headerKey, with: "")
        }
        return token
    }
    
    private func buildURL(resource: Resource,
                          scheme: String,
                          baseUrl: String,
                          extraUrl: String = "",
                          headerParameters: [String: String] = [:]) -> String {
        
        var components = URLComponents()
        components.scheme = scheme
        components.host = "\(baseUrl)\(resource.resource.route)\(extraUrl)"
        
        if headerParameters.count > 0 {
            var params = [URLQueryItem]()
            for (key,value) in headerParameters {
                params.append(URLQueryItem(name: key, value: value))
            }
            components.queryItems = params
        }
        
        return components.url!.absoluteString.replacingOccurrences(of: "%2F", with: "/")
    }
    


    
}
