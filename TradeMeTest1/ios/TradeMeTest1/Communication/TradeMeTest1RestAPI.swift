//
//  TradeMeTest1RestAPI.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import Foundation
import Alamofire

class TradeMeTest1RestAPI: NSObject {
    
    static let sharedInstance = TradeMeTest1RestAPI()
    
    //MARK: - Login
    
    func requestToken(consumerKey : String,
                      signature : String,
                      onResponse: @escaping (User) -> (),
                      onFailure: @escaping (Error) -> ()) {
        
        guard let isReachable = RestAPIManager.sharedInstance.isReachable, isReachable
        else {
            onFailure(TMTest1Error.networkIsNotReachable)
            return
        }
        
        let header = RestClient.requestTokenHeader(consumerKey: consumerKey, signature: signature)
        
        RestAPIManager.sharedInstance.requestData(resource: .requestToken,
                                                           baseUrl: ServerURL.oauth.rawValue,
                                                           headerParameters: ["scope":"MyTradeMeRead,MyTradeMeWrite"],
                                                           headers: header,
                                                           onResponse:
            { (data) in
                
                // "oauth_token=4418B062F79A4272A231CC1C41D1BCF4&oauth_token_secret=A6987FE9FDCE58F5962D72F6FBD1C837&oauth_callback_confirmed=true"
                
                var user : User? = nil
                
                if  let responseStr = String.init(data: data, encoding: .utf8) {
                    
                    if let oauth_token = TradeMeTest1RestAPI.extractTokenForHeader(header: "oauth_token", str: responseStr),
                        let oauth_token_secret = TradeMeTest1RestAPI.extractTokenForHeader(header: "oauth_token_secret", str: responseStr) {
                        
                        user = User(token: oauth_token, secret: oauth_token_secret)
                        onResponse(user!)
                    }
                }
                
        }) { (error) in
            TRACE("error:\(error)")
            onFailure(error)
        }
        
    }
    
    //MARK: - Categories
    
    func getCategories(rootCategory : String,
                       depth: Int,
                       onResponse: @escaping ([Category]) -> (),
                       onFailure: @escaping (Error) -> ()) {
        
        guard let isReachable = RestAPIManager.sharedInstance.isReachable, isReachable
            else {
                onFailure(TMTest1Error.networkIsNotReachable)
                return
        }
        
        // https://api.tmsandbox.co.nz/v1/Categories/0.json?depth=1
        
        RestAPIManager.sharedInstance.requestData(resource: .getCategories,
                                                  baseUrl: ServerURL.base.rawValue,
                                                  extraUrl: "/\(rootCategory).json",
            headerParameters:["depth":String(depth)],
            onResponse:
            { (json) in
                // json -> [Category]
                do {
                    let result = try JSONDecoder().decode(RootCategory.self, from: json)
                    TRACE("result:\(result)")
                    onResponse(result.categories)
                    
                } catch let error {
                    onFailure(error)
                }
                
        }) { (error) in
            onFailure(error)
        }
        
    }
    
    //MARK: - Search
    
    func getDetailForCategories(categories : [String],
                                rows: Int,
                                onResponse: @escaping ([ListItem]) -> (),
                                onFailure: @escaping (Error) -> ()) {
        
        guard let isReachable = RestAPIManager.sharedInstance.isReachable, isReachable
            else {
                onFailure(TMTest1Error.networkIsNotReachable)
                return
        }
        
        //  https://api.tmsandbox.co.nz/v1/Search/General.json?buy=All
        //&category=0339-&clearance=All&condition=All&expired=false&listed_as=All&page=1&pay=All
        //&photo_size=List&return_collections=false&return_did_you_mean=false&return_metadata=false
        //&rows=20&seller_status=All&shipping_method=All&sort_order=Default
        
        // ?rows=20&category=0339-
        
        // TODO complete full login
        let consumerKey = kDefaultConsumerKey
        let signature = kDefaultSignature
        
        let header = RestClient.authorizedHeader(consumerKey: consumerKey, signature: signature)
        let categoriesStr = categories.reduce("", {previous, value in previous + value })
        
        RestAPIManager.sharedInstance.requestData(resource: .searchInCategory,
                                                  baseUrl: ServerURL.base.rawValue,
                                                  extraUrl: ".json",
                                                  headerParameters:["rows":String(rows), "category":categoriesStr],
                                                  headers:header,
                                                  onResponse:
            { (json) in
                // json -> [ListItem]
                
                // TRACE("data:\(String.init(data: json, encoding: .utf8))")
                
                do {
                    let result = try JSONDecoder().decode(RootListItem.self, from: json)
                    //TRACE("result:\(result)")
                    onResponse(result.list)
                    
                } catch let error {
                    onFailure(error)
                }
                
        }) { (error) in
            onFailure(error)
        }
        
    }
    
    //MARK: - Listing
    
    func getListing(listingId : Int,
                    onResponse: @escaping (ListItemDetail) -> (),
                    onFailure: @escaping (TMTest1Error) -> ()) {
        
        guard let isReachable = RestAPIManager.sharedInstance.isReachable, isReachable
            else {
                onFailure(TMTest1Error.networkIsNotReachable)
                return
        }
        
        // TODO complete full login
        let consumerKey = kDefaultConsumerKey
        let signature = kDefaultSignature
        
        let header = RestClient.authorizedHeader(consumerKey: consumerKey, signature: signature)
        
        // https://api.trademe.co.nz/v1/Listings/{listingId}.{file_format}
        
        let headerParameters = ["increment_view_count":"false",
                                "question_limit":"20",
                                "return_member_profile":"false"]
        
        RestAPIManager.sharedInstance.requestJson(resource: .getListingItemDetail,
                                                  baseUrl: ServerURL.base.rawValue,
                                                  extraUrl: "/\(String(listingId)).json",
                                                  headerParameters:headerParameters,
                                                  headers:header,
                                                  onResponse:
            { (json) in
                // json -> ListItemDetail
                // TRACE("data:\(String.init(data: json, encoding: .utf8))")
                // TRACE("\(json)")
                
                if let result = ListItemDetail.init(json: json) {
                    onResponse(result)
                } else {
                    onFailure(TMTest1Error.dataFileNil)
                }
                
        }) { (error) in
            onFailure(error)
        }
        
    }
    
    //MARK: - Private
    
    private class func extractTokenForHeader(header:String, str:String) -> String? {
        
        let range = NSRange(str.startIndex..., in: str)
        var token: String? = nil
        let headerKey = "\(header)="
        if let regex = try? NSRegularExpression(pattern: "\(headerKey)([a-zA-Z0-9])*", options: .caseInsensitive),
            let match = regex.firstMatch(in: str, range: range) {
            
            token = String(str[Range(match.range, in: str)!]).replacingOccurrences(of: headerKey, with: "")
        }
        return token
    }
}
