//
//  UICornerImageView.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 08/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

@IBDesignable
class UICornerImageView: UIImageView {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 5.0
 
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if (self.layer.cornerRadius != cornerRadius) {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
}
