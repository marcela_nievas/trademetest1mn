//
//  ListingItemDetailTableViewCell.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 08/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class ListingItemDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var dataLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
    func setupView(data : String) {
        self.dataLabel.text = data
    }
    
}
