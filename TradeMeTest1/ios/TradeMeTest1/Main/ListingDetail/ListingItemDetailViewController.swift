//
//  ListingItemDetailViewController.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 08/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit
import ImageSlideshow
import AlamofireImage

class ListingItemDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let listingItemDetailDataCellIdentifier = "listingItemDetailDataCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newItemImageView: UIImageView!
    
    var listingItemData = [String]()
    
    var spinnerView : UIView? = nil
    
    private var listItemDetail : ListItemDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.newItemImageView.isHidden = true
        self.titleLabel.text = ""
        
        registerCollectionViewCells()
    }
    
    func registerCollectionViewCells() {
        let nib = UINib(nibName: "ListingItemDetailTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: listingItemDetailDataCellIdentifier)
    }
    
    func setupView(listItem : ListItem) {
        
        removeSpinnerView()
        presentSpinnerView()
        
        TradeMeTest1RestAPI.sharedInstance.getListing(listingId : listItem.listId,
                                                      onResponse:
            {  [unowned self] (listItemDetail) in
                
                self.removeSpinnerView()
                
                self.listItemDetail = listItemDetail
                
                DispatchQueue.main.async {
                    self.configureView()
                }
                
        }) { (error) in
            self.removeSpinnerView()
            self.presentMessageError(error: error)
        }
    }
    
    private func configureView() {
        assert(self.listItemDetail != nil, "self.listItemDetail is missing")
        
        self.newItemImageView.isHidden = !self.listItemDetail.isNew
        
        var photos = [AlamofireSource]()
        for photo in self.listItemDetail.photos {
            if let url = photo.valuePhoto?.medium {
                photos.append(AlamofireSource(urlString: url )!)
            }
        }
        
        if photos.count == 0 {
            slideshow.setImageInputs([ImageSource(imageString: "kiwi")!])
        } else {
            slideshow.setImageInputs(photos)
        }
        
        slideshow.slideshowInterval = 5.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.zoomEnabled = false
        slideshow.draggingEnabled = true
        slideshow.activityIndicator = DefaultActivityIndicator()
        
        self.titleLabel.text = self.listItemDetail.title
        
        configureTableView()
    }
    
    private func configureTableView () {
        
        self.listingItemData.removeAll()
        
        if  self.listItemDetail.subtitle.count > 0 {
            self.listingItemData.append(self.listItemDetail.subtitle)
        }
        if  self.listItemDetail.body.count > 0 {
            self.listingItemData.append(self.listItemDetail.body)
        }
        if let priceStr = self.listItemDetail.priceDisplay,
            priceStr.count > 0 {
            self.listingItemData.append("Price \(priceStr)")
        }
        if  self.listItemDetail.suburb.count > 0 {
            self.listingItemData.append("Suburb \(self.listItemDetail.suburb)")
        }
        if  self.listItemDetail.allowsPickups.count > 0 {
            self.listingItemData.append("AllowsPickups \(self.listItemDetail.allowsPickups)")
        }
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 200
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.reloadData()
        
    }
    
    // MARK: - Spinner
    
    func presentSpinnerView() {
        
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func removeSpinnerView() {
        
        if let spinnerView = self.spinnerView {
            UIViewController.removeSpinner(spinner: spinnerView)
            self.spinnerView = nil
        }
    }
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listingItemData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: listingItemDetailDataCellIdentifier,
                                                 for: indexPath) as? ListingItemDetailTableViewCell
        assert(cell != nil, "cannot obtain ListingItemDetailTableViewCell")
        
        if indexPath.row <= self.listingItemData.count {
            cell!.setupView(data: self.listingItemData[indexPath.row])
        }
        
        return cell!
    }
    
    // MARK: - UITableViewDelegate
    
    
    
}
