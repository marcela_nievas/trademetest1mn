//
//  MasterViewController.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {
    
    let categoryTitleCellIdentifier = "CategoryCell"
    
    var spinnerView : UIView? = nil
    var detailViewController: DetailViewController? = nil
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
        
        let nc = NotificationCenter.default
        
        nc.addObserver(self,
                       selector: #selector(networkIsReachableNow),
                       name: .networkReachabilityIsReachable,
                       object: nil)
        
        nc.addObserver(self,
                       selector: #selector(networkIsNotReachableNow),
                       name: .networkReachabilityIsNotReachable,
                       object: nil)
        
        refreshCategories()
    }
    
    @objc func networkIsReachableNow(){
        if CategoryManager.sharedInstance.categories.count == 0 {
            refreshCategories()
        }
    }
    
    @objc func networkIsNotReachableNow(){
        
    }
 
    // MARK: - Refreshing
    
    func refreshCategories() {
        
        removeSpinnerView()
        presentSpinnerView()
        
        CategoryManager.sharedInstance.refreshCategories { [unowned self] (categories) in
            self.tableView.reloadData()
            self.removeSpinnerView()
        }
        
    }
    
    // MARK: - Segues
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                
                if indexPath.row <= CategoryManager.sharedInstance.categories.count {
                    
                    let category = CategoryManager.sharedInstance.categories[indexPath.row]
                    let vc = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                    
                    vc.category = category
                    vc.navigationItem.title = category.name
                    vc.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                    vc.navigationItem.leftItemsSupplementBackButton = true
                }
            }
        }
    }
    
    // MARK: - Table View
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CategoryManager.sharedInstance.categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: categoryTitleCellIdentifier, for: indexPath) as? CategoryTableViewCell
        assert(cell != nil, "cannot obtain CategoryTableViewCell")
        
        if indexPath.row <= CategoryManager.sharedInstance.categories.count {
            cell!.category = CategoryManager.sharedInstance.categories[indexPath.row]
        }
        
        return cell!
    }
    
    // MARK: - Spinner
    
    func presentSpinnerView() {
        
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func removeSpinnerView() {
        
        if let spinnerView = self.spinnerView {
            UIViewController.removeSpinner(spinner: spinnerView)
            self.spinnerView = nil
        }
    }
    
}

