//
//  CategoryTableViewCell.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 07/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var categoryTitleLabel: UILabel!
    
    var category : Category? = nil {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.configureView()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureView() {
        if let category = self.category  {
            self.categoryTitleLabel.text = category.name
        } else {
            self.categoryTitleLabel.text = ""
        }
    }
    
}
