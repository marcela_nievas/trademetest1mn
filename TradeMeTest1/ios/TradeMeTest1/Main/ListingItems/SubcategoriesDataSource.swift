//
//  SubcategoriesDataSource.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 20/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class SubcategoriesDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {
    
    let listingItemCellIdentifier = "SubcategoryCellIdentifier"
    
    var categories = [Category]()
    
    var itemSelected : ((_ category: Category) -> ())? = nil
    
    func refreshSubcategories(for categoryNumber:String,
                      onSuccess : @escaping () -> (),
                      onFailure : @escaping (TMTest1Error) -> ()) {
        
        TradeMeTest1RestAPI.sharedInstance.getCategories(rootCategory: categoryNumber,
                                                         depth: 1,
                                                         onResponse:
            { [unowned self] categories in
                
                self.categories = categories
                onSuccess()
                
                
        }) { (error) in
            onFailure(TMTest1Error.networkError(error: error))
        }
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listingItemCellIdentifier, for: indexPath) as! SubcategoryCollectionViewCell
        
        if indexPath.row < categories.count {
            let category = self.categories[indexPath.row]
            cell.category = category
        }
        cell.configureView()
        return cell
    }
    
    //MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row < categories.count {
            let category = self.categories[indexPath.row]
            self.itemSelected!(category)
        }
        

    }
    
    //Use for size
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let desireWidth: CGFloat = UIDevice.current.userInterfaceIdiom == .pad ? 200.0 : 150.0
        let amountWidth: Int = Int(collectionView.frame.size.width / desireWidth)
        
        var width = collectionView.frame.size.width
        if amountWidth > 0 {
            width = collectionView.frame.size.width / CGFloat(amountWidth)
        }
        
        return CGSize.init(width: width, height: 50)
    }
    
    //Use for interspacing
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    
}
