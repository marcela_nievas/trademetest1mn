//
//  DetailViewController.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var subcategoriesCollectionView: UICollectionView!
    @IBOutlet var subcategoriesDataServices: SubcategoriesDataSource!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet var dataService: ListingItemsDataSource!
    
    
    var spinnerView : UIView? = nil
    
    var category : Category? = nil {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.configureView()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = nil
        collectionView.delegate = nil
    }
    
    func configureView() {
        
        collectionView.dataSource = nil
        collectionView.delegate = nil
        
        if let category = category {
            
            removeSpinnerView()
            presentSpinnerView()
            
            dataService.refreshItems(for: category,
                                     onSuccess:
                {  [unowned self] in
                    
                    self.removeSpinnerView()
                    
                    self.collectionView.dataSource = self.dataService
                    self.collectionView.delegate = self.dataService
                    
                    self.collectionView.reloadData()
                    
            }) { (error) in
                self.removeSpinnerView()
                self.presentMessageError(error: error)
            }
        }
        
        if let category = category {
            
            subcategoriesDataServices.refreshSubcategories(for: category.number,
                                                           onSuccess:
                {
                    self.subcategoriesCollectionView.dataSource = self.subcategoriesDataServices
                    self.subcategoriesCollectionView.delegate = self.subcategoriesDataServices
                    
                    self.subcategoriesCollectionView.reloadData()
                    
                    self.subcategoriesDataServices.itemSelected = self.subcategoryWasSelected(item:)
                    
            })
            { (error) in
                self.presentMessageError(error: error)
            }
        }
    }
    
    func subcategoryWasSelected(item:Category) {
       
        self.category = item
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    // MARK: - Spinner
    
    func presentSpinnerView() {
        
        spinnerView = UIViewController.displaySpinner(onView: self.view)
    }
    
    func removeSpinnerView() {
        
        if let spinnerView = self.spinnerView {
            UIViewController.removeSpinner(spinner: spinnerView)
            self.spinnerView = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "ListingItemDetailSegue" {
            
            if let vc = segue.destination as? ListingItemDetailViewController,
                let cell = sender as? ListingItemCollectionViewCell {
                
                vc.setupView(listItem: cell.listItem!)
            }
        }
    }
    
}

