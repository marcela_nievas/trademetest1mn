//
//  ListingItemsDataSource.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 07/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class ListingItemsDataSource: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout  {

    let listingItemCellIdentifier = "ListingItemCellIdentifier"
    
    var items = [ListItem]()
    
    func refreshItems(for category:Category,
                      onSuccess : @escaping () -> (),
                      onFailure : @escaping (TMTest1Error) -> ()) {
        
        TradeMeTest1RestAPI.sharedInstance.getDetailForCategories(categories: [category.number],
                                                                  rows: 20,
                                                                  onResponse:
            { [unowned self] (items) in
                
                self.items = items
                onSuccess()
                
        }) { (error) in
            onFailure(TMTest1Error.networkError(error: error))
        }
        
    }
    
    //MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: listingItemCellIdentifier, for: indexPath) as! ListingItemCollectionViewCell
        
        if indexPath.row < items.count {
            let item = items[indexPath.row]
            cell.listItem = item
        }
        
        return cell
    }

    //MARK: - UICollectionViewDelegate
    
    
    
    //Use for size
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let desireWidth: CGFloat = UIDevice.current.userInterfaceIdiom == .pad ? 200.0 : 150.0
        let amountWidth: Int = Int(collectionView.frame.size.width / desireWidth)
        
        var width = collectionView.frame.size.width
        if amountWidth > 0 {
            width = collectionView.frame.size.width / CGFloat(amountWidth)
        }
        
        return CGSize.init(width: width, height: width)
    }
    
    //Use for interspacing
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    

}
