//
//  SubcategoryCollectionViewCell.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 20/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

class SubcategoryCollectionViewCell: UICollectionViewCell {
 
    @IBOutlet weak var titleLabel: UILabel!
    
    var category : Category? = nil
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.titleLabel.text = ""
    }
    
    func configureView() {
      
        self.titleLabel.text = self.category!.name
        
    }
}
