//
//  ListingItemCollectionViewCell.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 07/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ListingItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var loadingDarkView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var thumbnailRequest: Request?
    
    var listItem : ListItem? = nil {
        didSet {
            DispatchQueue.main.async { [unowned self] in
                self.configureView()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        stopIndicator()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.titleLabel.text = ""
        self.idLabel.text = ""
        setDefaultImage()
    }
    
    func configureView() {
        reset()
        
        self.titleLabel.text = self.listItem?.title
        if let id = self.listItem?.listId{
            self.idLabel.text = "#\(id)"
        }
        
        if let url = self.listItem?.thumbnail {
            loadImage(url:url)
        }
    }
    
    func reset() {
        stopIndicator()
        setDefaultImage()
        thumbnailRequest?.cancel()
    }
    
    func loadImage(url:String) {
        
        if let image = ImageDownloadManager.sharedInstance.cachedImage(for: url) {
            populate(with: image)
        } else {
            startIndicator()
            thumbnailRequest = ImageDownloadManager.sharedInstance.retrieveImage(for: url,
                                                                        onCompletion:
                { (image) in
                    self.populate(with: image)
                    self.stopIndicator()
            }, onFailure: {
                self.setDefaultImage()
                self.stopIndicator()
            })
        }
    }
    
    func populate(with image: UIImage) {
        self.thumbnailImageView.image = image
    }
    
    func startIndicator() {
        loadingDarkView.isHidden = false
        loadingIndicator.startAnimating()
    }
    
    func stopIndicator() {
        loadingDarkView.isHidden = true
        loadingIndicator.stopAnimating()
    }
    
    func setDefaultImage() {
        self.thumbnailImageView.image = GlobalConstants.kiwiDefaultImage
    }
    
}
