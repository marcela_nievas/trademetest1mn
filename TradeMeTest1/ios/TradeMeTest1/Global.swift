//
//  Global.swift
//  TradeMeTest1
//
//  Created by Marcela Nievas on 06/11/2018.
//  Copyright © 2018 Marce. All rights reserved.
//

import UIKit

let kDefaultConsumerKey = "A1AC63F0332A131A78FAC304D007E7D1"
let kDefaultSignature = "EC7F18B17A062962C6930A8AE88B16C7"

let kTitle = "TradeMe Task1"
let kOk = "Ok"
let kNoConnectivityDefaultError = "There is no connectivity now. Please, try again later."
let kDefaultErrorMsg = "Something went wrong. Please, try again later."

struct GlobalConstants {
    static let kiwiDefaultImage : UIImage = UIImage(named: "kiwiGray")!
}
 
public func TRACE(_ message: String,
                  function: String = #function,
                  file: String = #file,
                  line: Int = #line) {
    
    #if DEBUG
    let f : String = NSString(string: file).lastPathComponent
    print("\(f) [\(line)] \(function) > \(message)")
    #else
    #endif
}

